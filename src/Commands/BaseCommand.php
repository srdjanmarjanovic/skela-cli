<?php

declare(strict_types=1);

namespace App\Commands;

use Symfony\Component\Console\Command\Command;

abstract class BaseCommand extends Command
{
}