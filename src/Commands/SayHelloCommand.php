<?php

declare(strict_types=1);

namespace App\Commands;

use App\Services\WhatToSayService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SayHelloCommand extends BaseCommand
{
    protected static $defaultName = 'hello:say';
    private WhatToSayService $service;

    public function __construct(WhatToSayService $service)
    {
        parent::__construct();

        $this->service = $service;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Hello ' . $this->service->whatToSay());

        return parent::SUCCESS;
    }
}