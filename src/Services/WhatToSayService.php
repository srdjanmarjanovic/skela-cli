<?php

declare(strict_types=1);

namespace App\Services;

class WhatToSayService
{
    public function whatToSay(): string {
        return 'World';
    }
}