<?php

use App\Commands\SayHelloCommand;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        SayHelloCommand::class => DI\autowire(),
    ]);
};
