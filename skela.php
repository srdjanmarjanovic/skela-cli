<?php

use App\Commands\SayHelloCommand;
use DI\ContainerBuilder;
use Dotenv\Dotenv;
use Symfony\Component\Console\Application;

require __DIR__.'/vendor/autoload.php';

$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

$containerBuilder = new ContainerBuilder();

$dependencies = require 'di-container.php';
$dependencies($containerBuilder);

$container = $containerBuilder->build();

$application = new Application();
$application->addCommands([
    $container->get(SayHelloCommand::class),
]);
$application->run();